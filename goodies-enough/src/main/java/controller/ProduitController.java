package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.Produit;
import model.ProduitVueSimple;
import repo.ProduitRepository;

@RestController
@RequestMapping("/api/")
@CrossOrigin
public class ProduitController {
	
	@Autowired
	ProduitRepository produitRepository;
	
	@RequestMapping(value="homepage", produces = { "application/json" }, method = RequestMethod.GET)
	public Iterable<Produit> findHomepage() {
		return produitRepository.findHomepageProducts();
	}
	
	@RequestMapping(value="admin/products", produces = { "application/json" }, method = RequestMethod.GET)
	public Iterable<Produit> findAll() {
        return produitRepository.findAll();
    }

	@RequestMapping(value="catalog",produces = { "application/json" }, method = RequestMethod.GET)
    public @ResponseBody List<ProduitVueSimple> getProduit() {

        List<Produit> list = (List<Produit>) produitRepository.findAll();
        List<ProduitVueSimple> trim = new ArrayList<ProduitVueSimple>();

        for (Produit produit : list) {
            trim.add(new ProduitVueSimple(produit));
        }

        return trim;

    }
	
	@RequestMapping(value="catalog/{categorie}/{id}",produces = { "application/json" }, method = RequestMethod.GET)
    public @ResponseBody List<Produit> findProduitsCategorie(@PathVariable Long id) {
        List<Produit> list = (List<Produit>) produitRepository.findByCategorieProduitId(id);
        return list;
    }
	
	@RequestMapping(value="catalog/produit/{produit}/{id}",produces = { "application/json" }, method = RequestMethod.GET)
    public @ResponseBody Produit findProduit(@PathVariable Long id) {
        return produitRepository.findById(id).get();
    }
	
	@RequestMapping("admin/product")
	public @ResponseBody Produit create(@Valid Produit produit, HttpServletResponse response) throws IOException {

		if (produitRepository.findByNom(produit.getNom()) != null) {
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
			response.sendError(417, "Erreur : Ce nom de produit existe déjà");
			return null;
		} else {
			return produitRepository.save(produit);
		}

	}
	
	
	@RequestMapping(value="admin/products/{id}", produces = { "application/json" }, method = RequestMethod.PUT)
    public @ResponseBody Produit update(@Valid Produit produit, @PathVariable Long id, HttpServletResponse response) throws IOException  {
        
//		Produit produitOrigin = produitRepository.findById(id).get();

        
//        produitOrigin.setNom(produit.getNom());
//        produitOrigin.setImage(produit.getImage());
//        produitOrigin.setDescription(produit.getDescription());
//        produitOrigin.setPrix(produit.getPrix());
//        produitOrigin.setPromo(produit.isPromo());
//        produitOrigin.setSurCommande(produit.isSurCommande());
//        produitOrigin.setQuantiteStock(produit.getQuantiteStock());
        
             
        return produitRepository.save(produit);
    }
	
	@DeleteMapping("admin/products")
    public String delete(Long id) {
        produitRepository.deleteById(id);
        return "HTTP DELETE was called";
    }
	
	
	@GetMapping("catalog/product/{id}")
	public ResponseEntity displayProduitById(@PathVariable Long id) {
        Produit produit = produitRepository.findAllById(id);
        if(produit == null) {
            return new ResponseEntity("No produit found", HttpStatus.NOT_FOUND);
        }    
        else {
            return new ResponseEntity<Produit>(produit, HttpStatus.OK);
        }
    }
}

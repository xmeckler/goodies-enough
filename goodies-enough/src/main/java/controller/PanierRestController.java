package controller;


import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.Commande;
import model.Panier;
import model.Produit;
import repo.CommandeRepository;
import repo.PanierRepository;
import repo.ProduitRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class PanierRestController {
	
	@Autowired
	private PanierRepository panierRepository;
	
	@Autowired
	private CommandeRepository commandeRepository;
	
	@RequestMapping(value = "/cart/{id}", produces = {"application/json"}, method = RequestMethod.GET)
	public Iterable<Panier> findItems(@PathVariable long id) {
		return panierRepository.findAllByCommandePanierId(id);
	}

	
	// public Panier addItem(@RequestBody int quantite, long produitPanier, long commandePanier, BindingResult bindingResult, HttpServletResponse response) {
	@RequestMapping(value = "/cart", produces = {"application/json"}, method = RequestMethod.POST)
	public Panier addItem(@RequestBody @Valid Panier panier, BindingResult bindingResult, HttpServletResponse response) {

		// Recherche des recommandations
		// Code Chrislove
		
		if (panier.getCommandePanier() == null) {

			// Vérification si la commande n'existe pas
			Commande commandeVide = new Commande();
			// TODO : Attention récupérer l'ID de l'utilisateur depuis la session d'authentification		 
			// commandeVide.setUtilisateurCommande();
			Commande commandeCourante = commandeRepository.save(commandeVide);
			panier.setCommandePanier(commandeCourante);

		} else {
			
			// Vérification si le produit est déjà dans la commande
			Panier produitExistant = panierRepository.findByCommandePanierIdAndProduitPanierId(panier.getCommandePanier().getId(), panier.getProduitPanier().getId()); 
			if (produitExistant != null) {
				int quantiteFinale = produitExistant.getQuantite() + panier.getQuantite();
				panier.setId(produitExistant.getId());
				panier.setQuantite(quantiteFinale);
			}

		}

		if (bindingResult.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return new Panier();
		}
		
		panierRepository.save(panier);
		response.setStatus(HttpServletResponse.SC_CREATED);
		return panier;

	}
	
	/*
	@DeleteMapping(value="/admin/avis")
    public String deleteAvis(long id) {
        avisRepository.deleteById(id);
        return "HTTP DELETE was called";
    }
    */
}

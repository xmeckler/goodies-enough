package controller;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.Avis;
import repo.AvisRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AvisController {
	
	@Autowired
	private AvisRepository avisRepository;
	
	@GetMapping("/avis")
	public Iterable<Avis> findAll() {
		return avisRepository.findAll();
	}
	
	@PostMapping("/avis")
	public Avis createAvis(@Valid Avis avis, BindingResult bindingResult, HttpServletResponse response) {
		if (bindingResult.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return new Avis();
		}
		avisRepository.save(avis);
		response.setStatus(HttpServletResponse.SC_CREATED);
		return avis;
	}
	
	@DeleteMapping(value="/admin/avis")
    public String deleteAvis(long id) {
        avisRepository.deleteById(id);
        return "HTTP DELETE was called";
    }
}

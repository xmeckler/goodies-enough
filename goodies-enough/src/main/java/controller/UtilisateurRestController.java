package controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.springframework.security.authentication.AuthenticationManager;
import model.Utilisateur;
import model.UtilisateurVueSimple;
import repo.UtilisateurRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class UtilisateurRestController {
	
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@RequestMapping(value = "admin/users", produces = {"application/json"}, method=RequestMethod.GET)
	public @ResponseBody List<UtilisateurVueSimple> findAll() {

		List<Utilisateur> listeUtilisateur = (List<Utilisateur>) utilisateurRepository.findAll();
		List<UtilisateurVueSimple> listeSimple = new ArrayList<UtilisateurVueSimple>();
		
		for (Utilisateur utilisateur : listeUtilisateur) {
			listeSimple.add(new UtilisateurVueSimple(utilisateur));
		}
		
		return listeSimple;
	}

	@RequestMapping(value = "users/user/{id}", produces = {"application/json"}, method=RequestMethod.GET)
	public @ResponseBody UtilisateurVueSimple findUserById(@PathVariable long id, HttpServletResponse response) throws IOException {
		
		try {
			Utilisateur utilisateur = utilisateurRepository.findById(id).get();
			UtilisateurVueSimple utilisateurSimple = new UtilisateurVueSimple();
			return utilisateurSimple;
		} catch (Exception e) {
			switch (e.getMessage()) {
				case "No value present":
					response.setStatus(HttpServletResponse.SC_NOT_FOUND);
					response.sendError(404, "Erreur : Utilisateur introuvable");
					break;
			}
		}
		return null;
	}
	

	@RequestMapping(value = "users", produces = { "application/json" }, method = RequestMethod.POST)
	public @ResponseBody Utilisateur create(@Valid Utilisateur utilisateur, @RequestParam("confirmMotDePasse") String confirmMotDePasse, HttpServletResponse response) throws IOException {
		
		if (!confirmMotDePasse.equals(utilisateur.getMotDePasse())) {
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
			response.sendError(417, "Erreur : Les mots de passe ne correspondent pas");
			return null;
		} else if (utilisateurRepository.findAllByEmail(utilisateur.getEmail()) != null) {
			response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);
			response.sendError(417, "Erreur : Cet utilisateur existe déjà");
			return null;
		} else {
			return utilisateurRepository.save(utilisateur);
		}
	}
	
	@RequestMapping(value="admin/users/{id}", produces = { "application/json" }, method = RequestMethod.PUT)
	public @ResponseBody Utilisateur update(@Valid Utilisateur utilisateur, @PathVariable long id,  HttpServletResponse response) throws IOException  {
		return utilisateurRepository.save(utilisateur);
	}

	@DeleteMapping(value="admin/users/user/delete")
	public String delete(long id) {
		utilisateurRepository.deleteById(id);
		return "HTTP DELETE was called";
	}
	
	
	@RequestMapping(value="login", method=RequestMethod.POST)
    public ResponseEntity login(@RequestBody Utilisateur utilisateur,  HttpServletResponse response) throws IOException, AuthenticationException {
		Utilisateur login = new Utilisateur();
		String message;
		login = utilisateurRepository.findByEmail(utilisateur.getEmail());
		if(login != null) {
			if(utilisateur.getEmail().equals(login.getEmail()) && utilisateur.getMotDePasse().equals(login.getMotDePasse())) {
				String tokenUser = "";
				System.out.println("passe");
				try {
					Algorithm algorithm = Algorithm.HMAC256("secret");	    		
					tokenUser = JWT.create().withClaim("email", utilisateur.getEmail()).withClaim("motDePasse", utilisateur.getMotDePasse()).sign(algorithm);
				} catch (JWTCreationException exception){
					return new ResponseEntity(exception.getMessage(), HttpStatus.FORBIDDEN);
				}
				String tokens = "{"+'"'+"tokenUser"+'"'+":"+'"'+tokenUser+'"'+"}";
				return new ResponseEntity( tokens, HttpStatus.ACCEPTED) ;
			}
			message = "Mauvais mot de passe";
			return new ResponseEntity(message, HttpStatus.FORBIDDEN);
		} else if (utilisateur.getEmail().equals("admin") && utilisateur.getMotDePasse().equals("admin")) {
			String tokenAdmin = "";
			try {
				Algorithm algorithm = Algorithm.HMAC256("secret");	    		
				tokenAdmin = JWT.create().withClaim("email", "admin").withClaim("motDePasse", "admin").sign(algorithm);
			} catch (JWTCreationException exception){
				return new ResponseEntity(exception.getMessage(), HttpStatus.FORBIDDEN);
			}
			String tokens = "{"+'"'+"tokenAdmin"+'"'+":"+'"'+tokenAdmin+'"'+"}";
			return new ResponseEntity( tokens, HttpStatus.ACCEPTED) ;
		}
		message = "Cet email ne correspond à aucun utilisateur";
		return new ResponseEntity(message, HttpStatus.NOT_FOUND);

    }
	
	@RequestMapping(value = "estConnecte", method = RequestMethod.GET)
	public @ResponseBody Utilisateur estConnecte(Principal utilisateurConnecte) { 
		System.out.println(utilisateurConnecte);
		try {
			Utilisateur utilisateur = utilisateurRepository.findByEmail(utilisateurConnecte.getName());
			return utilisateur;
		} catch (NullPointerException e) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Erreur Email", e);
		}
	}
}

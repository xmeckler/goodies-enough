package controller;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter{
	private String tokenHeader;

    public JwtAuthorizationTokenFilter(@Value("AUTHORIZATION") String tokenHeader) {
        this.tokenHeader = tokenHeader;
    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

        final String requestHeader = request.getHeader(this.tokenHeader);
        
        String authToken = null;
        DecodedJWT jwt = null;
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
            authToken = requestHeader.substring(7);
            try {
                Algorithm algorithm = Algorithm.HMAC256("secret");
                JWTVerifier verifier = JWT.require(algorithm).build();
                jwt = verifier.verify(authToken);
            } catch (JWTVerificationException exception){
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.sendError(403, "Token invalide : accès refusé");
            }
            
        } else {
            System.out.println("couldn't find bearer string, will ignore the header");
        }

        if (jwt != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(jwt.getClaim("email").asString(), jwt.getClaim("motDePasse").asString());
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }
}

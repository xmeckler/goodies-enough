package controller;


import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import model.Commande;
import model.Panier;
import model.Produit;
import model.Utilisateur;
import repo.CommandeRepository;
import repo.PanierRepository;


@RestController
@CrossOrigin
@RequestMapping("/api/")
public class CommandeRestController {

	@Autowired
	private CommandeRepository commandeRepository;

	@Autowired
	private PanierRepository panierRepository;
	
	@RequestMapping(value = "admin/orders",produces = {"application/json"}, method=RequestMethod.GET)
	public Iterable<Commande> findAll() {
		return commandeRepository.findAll();
	}

	@RequestMapping(value = "orders/order/{id}",produces = {"application/json"}, method=RequestMethod.GET)
	public @ResponseBody Commande findByIdUser(@PathVariable long id) {
		return commandeRepository.findById(id).get();
	}

	@RequestMapping(value = "admin/orders/order/{id}",produces = {"application/json"}, method=RequestMethod.GET)
	public @ResponseBody Commande findByIdAdmin(@PathVariable long id) {
		return commandeRepository.findById(id).get();
	}
	
	@RequestMapping(value = "orders", produces = { "application/json" }, method = RequestMethod.POST)
	public @ResponseBody Commande createOrder(@Valid Commande commande, HttpServletResponse response) throws IOException {
		return commandeRepository.save(commande);
	}
	
	
	@RequestMapping(value = "orders/{id}", produces = {"application/json"}, method = RequestMethod.PATCH)
	public @ResponseBody Commande finalizeOrder(@Valid Commande commande, @PathVariable long id, HttpServletResponse response) throws IOException {
		Commande commandeOrigine = commandeRepository.findById(id).get();
		
        String[] modesPaiement = {"CB", "CHEQUE", "ESPECE", "PAYPAL", "LINGOTS", "CONFITURES"};
        int min = 0, max = modesPaiement.length;
        int rnd = min + (int)(Math.random() * (max - min));
        String modePaiement = modesPaiement[rnd];
		
		// Lister les produits du panier au format json, correspondant à la commande
		// Injecter le contenu json (en String) dans le contenuCommande
		
        int montantTotal = 0;
        int nombreProduit = 0;
		Iterable<Panier> produits = panierRepository.findAllByCommandePanierId(id);
		// String contenuCommande = JSON.Stringify(produits);
		for (Panier produit : produits) {
			
			// Calcul du montant total du panier
			Produit produitPanier = produit.getProduitPanier();
			montantTotal += (produit.getQuantite() * produitPanier.getPrix());
			
			// Calcul du nombre total de produits
			nombreProduit += produit.getQuantite();
			
		}
		
        
        
        
		commandeOrigine.setDatePaiement(new Date());
		commandeOrigine.setModePaiement(modePaiement);
		commandeOrigine.setNbProduit(nombreProduit);
		commandeOrigine.setMontantTotal(montantTotal);
		commandeOrigine.setContenuCommande(commande.getContenuCommande());
		
		return commandeRepository.save(commandeOrigine);
	}
	
	
	
	@RequestMapping(value = "admin/orders/validate/{id}", produces = {"application/json"}, method = RequestMethod.PATCH)
	public @ResponseBody Commande validate(@PathVariable long id, HttpServletResponse response) throws IOException {
		Commande commandeOrigin = commandeRepository.findById(id).get();
		
		// Génération automatique du nouveau numéro de commande 
		Commande derniereCommande = commandeRepository.findFirstByOrderByDateValideDesc();
		String numCommande = "CMD-GE-";
		int numCommandeSuivant = Integer.parseInt(derniereCommande.getNumCommande().substring(7)) +1 ;
		int nbrZero = String.valueOf(numCommandeSuivant).length();
		for(int i=0;i<nbrZero;i++) {
			numCommande += "0";
		}
		numCommande += numCommandeSuivant;

		commandeOrigin.setNumCommande(numCommande);
		commandeOrigin.setDateValide(new Date());
		commandeOrigin.setStatutCommande(true);		
		
		// Envoi de mail au client
		
		return commandeRepository.save(commandeOrigin);
	}
	
	@RequestMapping(value = "admin/orders/refuse/{id}", produces = {"application/json"}, method = RequestMethod.PATCH)
	public @ResponseBody Commande refuse(@PathVariable long id, @RequestParam String motifRefus, HttpServletResponse response) throws IOException {
		Commande commandeOrigin = commandeRepository.findById(id).get();
		
		commandeOrigin.setMotifRefus(motifRefus);
		commandeOrigin.setDateRefus(new Date());
		commandeOrigin.setStatutCommande(true);
		
		// Envoi de mail au client
		
		return commandeRepository.save(commandeOrigin);
	}

	/*
	@RequestMapping(value="admin/orders", method = RequestMethod.DELETE)
	public String delete(long id) {
		commandeRepository.deleteById(id);
		return "HTTP DELETE was called";
	}
	*/
	
}

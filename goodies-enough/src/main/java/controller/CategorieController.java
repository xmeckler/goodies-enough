package controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.Categorie;
import repo.CategorieRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CategorieController {

	@Autowired
	private CategorieRepository categorieRepository;

	@GetMapping("/tests/categories")
	public Iterable<Categorie> findAll() {
		return categorieRepository.findAll();
	}

	@PostMapping("/admin/categories")
	public Categorie createCategorie(@Valid Categorie categorie, BindingResult bindingResult, HttpServletResponse response) {
		if (bindingResult.hasErrors()) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return new Categorie();
		}
		categorieRepository.save(categorie);
		response.setStatus(HttpServletResponse.SC_CREATED);
		return categorie;
	}
	
	@RequestMapping(value="/admin/categories/{id}", produces = { "application/json" }, method = RequestMethod.PUT)
    public @ResponseBody Categorie updateCategorie(@Valid Categorie categorie, @PathVariable("id") long id,  HttpServletResponse response) throws IOException  {
        Categorie categorieOrigin = categorieRepository.findById(id).get();
        categorieOrigin.setNom(categorie.getNom());
        
        categorieRepository.save(categorieOrigin);
        return categorieOrigin;
    }
	
	@DeleteMapping(value="/admin/categories")
    public String deleteCategorie(long id) {
        categorieRepository.deleteById(id);
        return "HTTP DELETE was called";
    }
}

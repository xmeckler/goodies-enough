package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Commande {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	@Column(nullable = false)
	private Date dateCommande = new Date();
	
	@Column(nullable = true)
	private Date datePaiement;

	@Column(nullable = true)
	private Date dateValide;

	@Column(nullable = true)
	private Date dateRefus;
	
	@Column(nullable = true)
	private String numCommande;
	
	@Column(nullable = true)
	private int nbProduit;
	
	@Column(nullable = true)
	private int montantTotal;

	@Column(nullable = true, columnDefinition="TEXT")
	private String contenuCommande;

	@Column(nullable = true)
	private String modePaiement;

	@Column(nullable = true)
	private String motifRefus;
	
	@Column(nullable = false)
	private boolean statutCommande = false;
	
	// @JsonIgnore
	@ManyToOne(cascade={CascadeType.MERGE})
	private Utilisateur utilisateurCommande;

	@JsonIgnore
	@OneToMany(mappedBy="commandePanier", fetch = FetchType.LAZY)
	private List<Panier> paniers = new ArrayList<Panier>();
	
	public Commande(Date dateCommande, Date datePaiement, Date dateValide, Date dateRefus, String numCommande, int nbProduit, int montantTotal, String contenuCommande, String modePaiement, String motifRefus, boolean statutCommande, Utilisateur utilisateur) {
		this.dateCommande = dateCommande;
		this.datePaiement = datePaiement;
		this.dateValide = dateValide;
		this.dateRefus = dateRefus;
		this.numCommande = numCommande;
		this.nbProduit = nbProduit;
		this.montantTotal = montantTotal;
		this.contenuCommande = contenuCommande;
		this.modePaiement = modePaiement;
		this.motifRefus = motifRefus;
		this.statutCommande = statutCommande;
		this.utilisateurCommande = utilisateur;
	}
	
	public Commande() {
	
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Date getDatePaiement() {
		return datePaiement;
	}

	public void setDatePaiement(Date datePaiement) {
		this.datePaiement = datePaiement;
	}

	public Date getDateValide() {
		return dateValide;
	}

	public void setDateValide(Date dateValide) {
		this.dateValide = dateValide;
	}

	public Date getDateRefus() {
		return dateRefus;
	}

	public void setDateRefus(Date dateRefus) {
		this.dateRefus = dateRefus;
	}

	public String getNumCommande() {
		return numCommande;
	}

	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}

	public int getNbProduit() {
		return nbProduit;
	}

	public void setNbProduit(int nbProduit) {
		this.nbProduit = nbProduit;
	}

	public int getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(int montantTotal) {
		this.montantTotal = montantTotal;
	}

	public String getContenuCommande() {
		return contenuCommande;
	}

	public void setContenuCommande(String contenuCommande) {
		this.contenuCommande = contenuCommande;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}

	public String getMotifRefus() {
		return motifRefus;
	}

	public void setMotifRefus(String motifRefus) {
		this.motifRefus = motifRefus;
	}

	public boolean isStatutCommande() {
		return statutCommande;
	}

	public void setStatutCommande(boolean statutCommande) {
		this.statutCommande = statutCommande;
	}

	public Utilisateur getUtilisateurCommande() {
		return utilisateurCommande;
	}

	public void setUtilisateurCommande(Utilisateur utilisateurCommande) {
		this.utilisateurCommande = utilisateurCommande;
	}

	public List<Panier> getPaniers() {
		return paniers;
	}

	public void setPaniers(List<Panier> paniers) {
		this.paniers = paniers;
	}

	public void addPanier(Panier panier) {
		paniers.add(panier);
	}
	
	public String toString() {
		return "Commande [id=" + id + ", dateCommande=" + dateCommande + ", datePaiement=" + datePaiement
				+ ", dateValide=" + dateValide + ", dateRefus=" + dateRefus + ", numCommande=" + numCommande + ", nbProduit=" + nbProduit
				+ ", montantTotal=" + montantTotal + ", contenuCommande=" + contenuCommande + ", modePaiement="
				+ modePaiement + ", motifRefus=" + motifRefus + ", statutCommande=" + statutCommande + "]";
	}


}

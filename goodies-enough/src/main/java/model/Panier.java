package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Panier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false)
	@Min(1)
	private int quantite;
	
	// @JsonIgnore
	@ManyToOne(cascade={CascadeType.MERGE})
	private Produit produitPanier;

	// @JsonIgnore
	@ManyToOne(cascade={CascadeType.MERGE})
	private Commande commandePanier;
	

	public Panier(int quantite, Produit produitPanier, Commande commandePanier) {
		this.quantite = quantite;
		this.produitPanier = produitPanier;
		this.commandePanier = commandePanier;
	}
	
	public Panier() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
	public Produit getProduitPanier() {
		return produitPanier;
	}

	public void setProduitPanier(Produit produitPanier) {
		this.produitPanier = produitPanier;
	}

	public Commande getCommandePanier() {
		return commandePanier;
	}

	public void setCommandePanier(Commande commandePanier) {
		this.commandePanier = commandePanier;
	}

	public String toString() {
		return "Panier [id=" + id + ", quantite=" + quantite + ", id_commande=" + commandePanier.getId() + ", commandePanier=" + commandePanier + ", produitPanier=" + produitPanier + "]";
	}

}

package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="categorie")
public class Categorie {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(unique=true)
	private String nom;	
	
	@Column(unique=true)
	private String slug;	

	@JsonIgnore
	// @LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy="categorieProduit", fetch = FetchType.LAZY)
	private List<Produit> produits = new ArrayList<Produit>();
	

	public Categorie(long id, String nom) {
		this.id = id;
		this.nom = nom;
	}

	public Categorie(String nom) {
		this.nom = nom;
	}	
	
	public Categorie() {
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String nom) {
		this.slug = nom.toLowerCase();
	}
	
	public void addProduits(Produit produit) {
		produits.add(produit);
	}
	
	public List<Produit> getProduits() {
		return produits;
	}

	public void setProduits(List<Produit> produits) {
		this.produits = produits;
	}


	
	public String toString() {
		return "Categorie [id=" + id + ", nom=" + nom + ", slug=" + slug + "]";
	}
	
}

package model;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class UtilisateurAdresse {

	@NotEmpty(message = "Champ obligatoire.")
	@Pattern(regexp="^(?=.*[0-9]){5}$", message="Code postal invalide.")
	@Column(nullable = false)
	private String codePostal;

	@NotEmpty(message = "Champ obligatoire.")
	@Column(nullable = false)
	private String ville;
	
	@NotEmpty(message = "Champ obligatoire.")
	@Column(nullable = false)
	private String numRue;
	
	@NotEmpty(message = "Champ obligatoire.")
	@Column(nullable = false)
	private String rue;
	
	@NotEmpty(message = "Champ obligatoire.")
	@Column(nullable = false)
	private String pays;
	
	
	public UtilisateurAdresse(String codePostal, String ville, String numRue, String rue, String pays) {
		this.codePostal = codePostal;
		this.ville = ville;
		this.numRue = numRue;
		this.rue = rue;
		this.pays = pays;
	}
	
	public UtilisateurAdresse() {
		
	}
	
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getNumRue() {
		return numRue;
	}
	public void setNumRue(String numRue) {
		this.numRue = numRue;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	@Override
	public String toString() {
		return "UtilisateurAdresse [codePostal=" + codePostal + ", ville=" + ville + ", numRue=" + numRue + ", rue="
				+ rue + ", pays=" + pays + "]";
	}
	
	
}

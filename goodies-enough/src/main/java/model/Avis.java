package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Avis {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private int note;
	
	@Column(nullable = false, columnDefinition="TEXT")
	private String commentaire;
	
	@Column
	private boolean valide = false;

	@JsonIgnore
	@ManyToOne(cascade={CascadeType.MERGE})
	private Produit produit;

	@JsonIgnore
	@ManyToOne(cascade={CascadeType.MERGE})
	private Utilisateur utilisateurAvis;
	
	public Avis(int note, String commentaire, Produit produit, Utilisateur utilisateur) {
		this.note = note;
		this.commentaire = commentaire;
		this.produit = produit;
		this.utilisateurAvis = utilisateur;
	}
	
	public Avis() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public boolean isValide() {
		return valide;
	}

	public void setValide(boolean valide) {
		this.valide = valide;
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public Utilisateur getUtilisateur() {
		return utilisateurAvis;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateurAvis = utilisateur;
	}

	public String toString() {
		return "Avis [id=" + id + ", note=" + note + ", commentaire=" + commentaire + ", valide=" + valide + ", produit=" + produit + ", utilisateur=" + utilisateurAvis + "]";
	}

}

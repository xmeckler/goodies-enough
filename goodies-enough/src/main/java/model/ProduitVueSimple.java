package model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class ProduitVueSimple {
	//Attributs Classe
		
		private long id;

		private String nom;
		
		private String slug;
		
		private String image;

		
		private String description;

		
		private int prix;

		
		private boolean promo;

		
		private boolean surCommande;

		
		private int quantiteStock;

		//private Categorie categorie;

		//Constructeurs

		public ProduitVueSimple(Produit produit) {
			this.id = produit.getId();
			this.nom = produit.getNom();
			this.slug = produit.getSlug();
			this.image = produit.getImage();
			this.description = produit.getDescription();
			this.prix = produit.getPrix();
			this.promo = produit.isPromo();
			this.surCommande = produit.isSurCommande();
			this.quantiteStock = produit.getQuantiteStock();
		}


		public ProduitVueSimple() {

		}

		//Accesseurs
		
		public long getId() {
			return id;
		}



		public void setId(long id) {
			this.id = id;
		}



		public String getNom() {
			return nom;
		}



		public void setNom(String nom) {
			this.nom = nom;
		}



		public String getImage() {
			return image;
		}



		public void setImage(String image) {
			this.image = image;
		}



		public String getDescription() {
			return description;
		}



		public void setDescription(String description) {
			this.description = description;
		}



		public double getPrix() {
			return prix;
		}



		public void setPrix(int prix) {
			this.prix = prix;
		}



		public boolean isPromo() {
			return promo;
		}



		public void setPromo(boolean promo) {
			this.promo = promo;
		}



		
		public boolean isSurCommande() {
			return surCommande;
		}



		public void setSurCommande(boolean surCommande) {
			this.surCommande = surCommande;
		}



		public int getQuantiteStock() {
			return quantiteStock;
		}



		public void setQuantiteStock(int quantiteStock) {
			this.quantiteStock = quantiteStock;
		}


		public String getSlug() {
			return slug;
		}


		public void setSlug(String slug) {
			this.slug = slug;
		}


		@Override
		public String toString() {
			return "Produit [id=" + id + ", nom=" + nom + ", image=" + image + ", description=" + description + ", prix="
					+ prix + ", promo=" + promo + ", surCommande=" + surCommande
					+ ", quantiteStock=" + quantiteStock + "]";
		}






}

package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Produit {

	//Attributs Classe
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(nullable=false, unique=true)
	private String nom;

	@Column(nullable=false, unique=true)
	private String slug;

	@NotEmpty
	@Column(nullable=false, columnDefinition="TEXT")
	private String image;

	@NotEmpty
	@Column(nullable=false, columnDefinition="TEXT")
	private String description;

	@NotNull
	@Column(nullable=false)
	private int prix;

	@Column(nullable=false)
	private boolean promo=false;

	@Column(nullable=false)
	private int nbVente=0;

	@Column(nullable=false)
	private boolean surCommande;

	@Column(nullable=false)
	private int quantiteStock;

	@ManyToOne(cascade={CascadeType.MERGE})
	private Categorie categorieProduit;

	@JsonIgnore
	@OneToMany(mappedBy="produit", fetch = FetchType.LAZY)
	private List<Avis> listeAvis = new ArrayList<Avis>();
	
	@JsonIgnore
	@OneToMany(mappedBy="produitPanier", fetch = FetchType.LAZY)
	private List<Panier> paniers = new ArrayList<Panier>();

	public Produit(String nom, String slug, String image, String description, int prix, boolean promo, int nbVente, boolean surCommande, int quantiteStock, Categorie categorie) {
		this.nom = nom;
		this.slug = slug;
		this.image = image;
		this.description = description;
		this.prix = prix;
		this.promo = promo;
		this.nbVente = nbVente;
		this.surCommande = surCommande;
		this.quantiteStock = quantiteStock;
		this.categorieProduit = categorie;
	}

	public Produit() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean isPromo() {
		return promo;
	}

	public void setPromo(boolean promo) {
		this.promo = promo;
	}

	public int getNbVente() {
		return nbVente;
	}

	public void setNbVente(int nbVente) {
		this.nbVente = nbVente;
	}

	public boolean isSurCommande() {
		return surCommande;
	}

	public void setSurCommande(boolean surCommande) {
		this.surCommande = surCommande;
	}

	public int getQuantiteStock() {
		return quantiteStock;
	}

	public void setQuantiteStock(int quantiteStock) {
		this.quantiteStock = quantiteStock;
	}
	
	public Categorie getCategorie() {
		return categorieProduit;
	}

	public void setCategorie(Categorie categorie) {
		this.categorieProduit = categorie;
	}

	public void addAvis(Avis avis) {
		listeAvis.add(avis);
	}
	
	public List<Avis> getAvis() {
		return listeAvis;
	}

	public void setAvis(List<Avis> avis) {
		this.listeAvis = avis;
	}

	public List<Panier> getPaniers() {
		return paniers;
	}

	public void setPaniers(List<Panier> paniers) {
		this.paniers = paniers;
	}

	public void addPanier(Panier panier) {
		paniers.add(panier);
	}
	
	public String toString() {
		return "Produit [id=" + id + ", nom=" + nom + ", image=" + image + ", description=" + description + ", prix="
				+ prix + ", promo=" + promo + ", nbVente=" + nbVente + ", surCommande=" + surCommande
				+ ", quantiteStock=" + quantiteStock + ", categorie=" + categorieProduit + "]";
	}

}

package model;

import java.util.Date;


public class UtilisateurVueSimple {

	private long id;
	private String nom;
	private String prenom;
	private String email;
	private String numTelephone;
	private UtilisateurAdresse utilisateurAdresse;
	private Date dateInscription = new Date();
	
	public UtilisateurVueSimple(String nom, String prenom, String email, String numTelephone,
			UtilisateurAdresse utilisateurAdresse, Date dateInscription) {
	
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.numTelephone = numTelephone;
		this.utilisateurAdresse = utilisateurAdresse;
		this.dateInscription = dateInscription;
	}
	
	public UtilisateurVueSimple(Utilisateur utilisateur) {
		
		this.id = utilisateur.getId();
		this.nom = utilisateur.getNom();
		this.prenom = utilisateur.getPrenom();
		this.email = utilisateur.getEmail();
		this.numTelephone = utilisateur.getNumTelephone();
		this.utilisateurAdresse = utilisateur.getUtilisateurAdresse();
		this.dateInscription = utilisateur.getDateInscription();
	}

	public UtilisateurVueSimple() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumTelephone() {
		return numTelephone;
	}

	public void setNumTelephone(String numTelephone) {
		this.numTelephone = numTelephone;
	}

	public UtilisateurAdresse getUtilisateurAdresse() {
		return utilisateurAdresse;
	}

	public void setUtilisateurAdresse(UtilisateurAdresse utilisateurAdresse) {
		this.utilisateurAdresse = utilisateurAdresse;
	}

	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	@Override
	public String toString() {
		return "UtilisateurVueSimple [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email
				+ ", numTelephone=" + numTelephone + ", utilisateurAdresse=" + utilisateurAdresse + ", dateInscription="
				+ dateInscription + "]";
	}
	
	
}

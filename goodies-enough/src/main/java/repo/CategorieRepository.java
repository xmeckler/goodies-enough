package repo;


import org.springframework.data.jpa.repository.JpaRepository;

import model.Categorie;

public interface CategorieRepository extends JpaRepository<Categorie, Long> {
 }

package repo;

import org.springframework.data.jpa.repository.JpaRepository;

import model.Avis;

public interface AvisRepository extends JpaRepository<Avis, Long> {

}

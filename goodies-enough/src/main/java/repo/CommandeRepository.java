package repo;

import org.springframework.data.repository.CrudRepository;

import model.Commande;



public interface CommandeRepository  extends CrudRepository<Commande, Long>{
	Commande findFirstByOrderByDateValideDesc();
}

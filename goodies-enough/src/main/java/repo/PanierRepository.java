package repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import model.Panier;

public interface PanierRepository extends JpaRepository<Panier, Long> {

	List<Panier> findAllByCommandePanierId(long id);

	// @Query("SELECT p FROM panier p WHERE p.commande_panier_id = ?1 AND p.produit_panier_id = ?2")
	Panier findByCommandePanierIdAndProduitPanierId(long id_commande, long id_produit);
	
}

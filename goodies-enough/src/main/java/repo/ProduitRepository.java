package repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import model.Produit;

public interface ProduitRepository extends JpaRepository<Produit,Long>{
	Produit findByNom(String nom);
	Produit findAllById(Long id);
	
	@Query(value = "select * from Produit p where p.promo = 1 order by rand() limit 0,5", nativeQuery = true)
	Iterable<Produit> findHomepageProducts();
	
	Iterable<Produit> findTop10ByCategorieProduitIdOrderByNbVenteDesc(Long id);
	
	Iterable<Produit> findByCategorieProduitId(Long id);
	
}

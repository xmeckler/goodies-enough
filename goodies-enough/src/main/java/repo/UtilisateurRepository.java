package repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import model.Utilisateur;
import model.UtilisateurVueSimple;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long>{
	
	Utilisateur findByEmail(String email);
	UtilisateurVueSimple findAllByEmail(String email);
	

}

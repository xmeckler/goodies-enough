package com.goodiesenough.apside.goodiesenough;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.SpringApplication;

@EnableJpaRepositories("repo")
@ComponentScan({"controller", "config"})
@EntityScan("model")
@SpringBootApplication
public class GoodiesEnoughApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodiesEnoughApplication.class, args);
        
    }

}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../services/category.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  categories: Array<any>;
  products: Array<any>;
  bestsale: Array<any>;
  private paramObs: any;
  public idCategorie: string;

  constructor(private route: ActivatedRoute, private categoryService: CategoryService, private productService: ProductService) { 
    route.params.subscribe(idCategorie => {
      this.refreshContent();
    });
  }

  ngOnInit() {

    this.refreshContent();

  }

  refreshContent() {
    // Récupération de l'id de la catégorie depuis l'URL
    this.paramObs = this.route.params.subscribe(params => {
      this.idCategorie = this.route.snapshot.paramMap.get('idCategorie');
    });

    // Récupération de la liste des catégories
    this.categoryService.findAll().subscribe(data => {
      this.categories = data;
    });

    if (this.idCategorie != null) {

      // Récupération des produits pour la catégorie en cours
      this.productService.findProduitsCategorie(this.idCategorie).subscribe(data => {
        this.products = data;
        this.bestsale = Object.assign([], data).sort((a, b) => b.nbVente - a.nbVente).slice(0, 10);
        // this.initializeState();
      });

    } else {

      // Récupération de la liste de tous les produits
      this.productService.findAll().subscribe(data => {
        this.products = data;
      });

    }
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'hammerjs';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';

// Angular Material
import {MatCheckboxModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';

import { BackendComponent } from './backendComponents/backend.component';
import { ListeCategorieComponent } from './backendComponents/categorie/liste-categorie/liste-categorie.component';
import { ListeProduitComponent } from './backendComponents/produit/liste-produit/liste-produit.component';
import { ListeUserComponent } from './backendComponents/user/liste-utilisateur/liste-user.component';
import { FrontendComponent } from './frontendComponents/frontend.component';
import { FormLoginComponent } from './frontendComponents/login/form-login/form-login.component';
import { FormCreateAccountComponent } from './frontendComponents/login/form-create-account/form-create-account.component';
import { FormForgotPasswordComponent } from './frontendComponents/login/form-forgot-password/form-forgot-password.component';
import { FicheProduitComponent } from './frontendComponents/fiche-produit/fiche-produit.component';
import { PanierComponent } from './frontendComponents/panier/panier.component';
import { CatalogueComponent } from './frontendComponents/catalogue/catalogue.component';
import { AddProduitComponent } from './backendComponents/produit/add-produit/add-produit.component';
import { NavGoodiesEnoughComponent } from './nav-goodies-enough/nav-goodies-enough.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ListeCommandeComponent } from './backendComponents/commande/liste-commande/liste-commande.component';


/* Génériques */
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainFooterComponent } from './main-footer/main-footer.component';
import { PopinAddedComponent } from './popin-added/popin-added.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { HomepageComponent }   from './homepage/homepage.component';

/* Statiques */
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { ContactComponent }         from './contact/contact.component';
import { SupportComponent }         from './support/support.component';
import { CgvComponent }             from './cgv/cgv.component';
import { FaqComponent }             from './faq/faq.component';

/* Formulaires */
import { RetrievePasswordComponent } from './retrieve-password/retrieve-password.component';
import { ResetPasswordComponent }    from './reset-password/reset-password.component';
import { SignupComponent }           from './signup/signup.component';
import { LoginComponent }            from './login/login.component';

/* Catalogue */
import { CatalogProductReviewComponent } from './catalog-product-review/catalog-product-review.component';
import { CatalogProductComponent }       from './catalog-product/catalog-product.component';
import { LeaveReviewComponent }          from './leave-review/leave-review.component';
import { CatalogComponent }              from './catalog/catalog.component';

/* Panier */
import { CartDetailsComponent } from './cart-details/cart-details.component';
import { CartThanksComponent } from './cart-thanks/cart-thanks.component';

/* Utilisateur */
import { UserAccountEditComponent } from './user-account-edit/user-account-edit.component';
import { UserListOrdersComponent }  from './user-list-orders/user-list-orders.component';
import { UserAccountComponent }     from './user-account/user-account.component';

/* Outil d'administration */
import { PopinCancelComponent } from './admin-goodies/popin-cancel/popin-cancel.component';
import { PopinDeleteComponent } from './admin-goodies/popin-delete/popin-delete.component';
import { CategoriesComponent }  from './admin-goodies/categories/categories.component';
import { ProductsComponent }    from './admin-goodies/products/products.component';
import { OrdersComponent }      from './admin-goodies/orders/orders.component';
import { UsersComponent }       from './admin-goodies/users/users.component';
import { AdminNavComponent }    from './admin-goodies/admin-nav/admin-nav.component';
import { HeaderComponent } from './backendComponents/header/header.component';
import { ViewCommandeComponent } from './backendComponents/commande/view-commande/view-commande.component';

/* Login */
import { AuthorizationService } from './services/auth.service';
import { httpInterceptorProviders } from './interceptor';

@NgModule({
    declarations: [
        AppComponent,
        MainHeaderComponent,
        MainFooterComponent,
        ListeCommandeComponent,
        CgvComponent,
        ContactComponent,
        FaqComponent,
        AddProduitComponent,
        NavGoodiesEnoughComponent,
        BackendComponent,
        ListeProduitComponent,
        MentionsLegalesComponent,
        MentionsLegalesComponent,
        SupportComponent,
        LoginComponent,
        SignupComponent,
        RetrievePasswordComponent,
        ResetPasswordComponent,
        CatalogComponent,
        CatalogProductComponent,
        HomepageComponent,
        PopinAddedComponent,
        CartDetailsComponent,
        BreadcrumbComponent,
        CartThanksComponent,
        UserAccountComponent,
        UserAccountEditComponent,
        UserListOrdersComponent,
        CategoriesComponent,
        PopinDeleteComponent,
        ProductsComponent,
        UsersComponent,
        OrdersComponent,
        PopinCancelComponent,
        LeaveReviewComponent,
        CatalogProductReviewComponent,
        AdminNavComponent,
        HeaderComponent,
        ListeCategorieComponent,
        ViewCommandeComponent,
        ListeUserComponent


    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        [MatButtonModule, MatCheckboxModule],
        BrowserModule,
        BrowserAnimationsModule,
        MatCheckboxModule,
        MatCheckboxModule,
        MatButtonModule,
        MatInputModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatRadioModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatMenuModule,
        MatSidenavModule,
        MatToolbarModule,
        MatListModule,
        MatGridListModule,
        MatCardModule,
        MatStepperModule,
        MatTabsModule,
        MatExpansionModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatIconModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatDialogModule,
        MatTooltipModule,
        MatSnackBarModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        HttpClientModule,
        LayoutModule,
        FormsModule

    ],
    providers: [httpInterceptorProviders],
    bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  adminAuth(): string {
    let adminToken = sessionStorage.getItem('tokenAdmin');
    return adminToken;
  }

  userAuth(): string {
    let userToken = sessionStorage.getItem('tokenUser');
    return userToken;
  }
}

import { Component, OnInit } from '@angular/core';
import { UserLoggedService } from '../services/user-logged.service';
import { Utilisateur } from '../model/utilisateur';

@Component({
  selector: 'user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {
  user: Utilisateur;
  constructor(private userLogged: UserLoggedService) { }

  ngOnInit() {
    this.userLogged.getUser().subscribe(data => {
        this.user = data;
        console.log(data);
    });
  }


  onClick(event: Event): void {
    sessionStorage.clear();
  }
}

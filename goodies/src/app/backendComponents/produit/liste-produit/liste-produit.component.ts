import { Component, OnInit, ViewChild } from '@angular/core';
import { ListeProduitService} from '../../../services/listeProduitAdmin/liste-produit.service';
import {Produit} from '../../../model/produit';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';

@Component({
  selector: 'app-liste-produit',
  templateUrl: './liste-produit.component.html',
  styleUrls: ['./liste-produit.component.css']
})
export class ListeProduitComponent implements OnInit {
  constructor(private listeProduitService: ListeProduitService) { }


  produits: Array<Produit>;
  displayedColumns: string[] = ['id', 'nom', 'prix', 'nb_vente', 'promo', 'quantite_stock', 'sur_commande', 'actions'];
  dataSource;

  getRecord(rowId: number) {
    console.log(rowId);
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    console.log(this.produits);
    this.listeProduitService.getAll().subscribe(data => {
      this.produits = data;
      this.dataSource = new MatTableDataSource(this.produits);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });


    console.log(this.produits);
  }
}

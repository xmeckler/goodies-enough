import { Component, OnInit } from '@angular/core';
import {Produit} from '../../../model/produit';
import { NgForm } from '@angular/forms';
import {Subscription} from 'rxjs';
import {ListeProduitService} from '../../../services/listeProduitAdmin/liste-produit.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Categorie} from '../../../model/categorie';
import {ListeCategorieService} from '../../../services/listeCategorieAdmin/liste-categorie.service';
import {MatTableDataSource} from '@angular/material';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-add-produit',
  templateUrl: './add-produit.component.html',
  styleUrls: ['./add-produit.component.css']
})
export class AddProduitComponent implements OnInit {
  produit: Produit = new Produit();
  categories: Array<Categorie>;

  constructor(private router: Router, private listeCategorieService: ListeCategorieService, private listeProduitService: ListeProduitService) { }


  ngOnInit() {
    this.listeCategorieService.getAll().subscribe(
      data => {
        this.categories = data;
      });
  }

  createProduit(): void {
    this.listeProduitService.createProduit(this.produit)
      .subscribe( data => {
        alert('Création du produit réussie');
      });
    this.router.navigate(['/admin/manage-products']);
  }
}

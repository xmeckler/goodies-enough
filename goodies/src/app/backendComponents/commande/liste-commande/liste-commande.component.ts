import {Component, OnInit, ViewChild} from '@angular/core';
import {Commande} from '../../../model/commande';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ListeCommandeService} from '../../../services/listeCommandeAdmin/liste-commande.service';

@Component({
  selector: 'app-liste-commande',
  templateUrl: './liste-commande.component.html',
  styleUrls: ['./liste-commande.component.css']
})
export class ListeCommandeComponent implements OnInit {

  constructor(private listeCommandeService: ListeCommandeService) { }

  commandes: Array<Commande>;
  displayedColumns: string[] = ['dateCommande', 'Client', 'montantTotal', 'nb_produits', 'actions'];
  dataSource;



  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.listeCommandeService.getAll().subscribe(data => {
      this.commandes = data;
      this.dataSource = new MatTableDataSource(this.commandes)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }



}

import {Component, OnInit, ViewChild} from '@angular/core';
import {Utilisateur} from '../../../model/utilisateur';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ListeUtilisateurService} from '../../../services/listeUserAdmin/liste-utilisateur.service';

@Component({
  selector: 'app-liste-user',
  templateUrl: './liste-user.component.html',
  styleUrls: ['./liste-user.component.css']
})
export class ListeUserComponent implements OnInit {

  constructor(private listeUtilisateurService: ListeUtilisateurService) {
  }


  utilisateurs: Array<Utilisateur>;
  displayedColumns: string[] = ['nom', 'nb_commande', 'recette', 'actions'];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    console.log(this.utilisateurs);
    this.listeUtilisateurService.getAll().subscribe(data => {
      this.utilisateurs = data;
      this.dataSource = new MatTableDataSource(this.utilisateurs);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

  }
}

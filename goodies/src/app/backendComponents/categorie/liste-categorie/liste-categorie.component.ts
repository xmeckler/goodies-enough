import { Component, OnInit, ViewChild } from '@angular/core';
import {ListeCategorieService} from '../../../services/listeCategorieAdmin/liste-categorie.service';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import {Categorie} from '../../../model/categorie';

@Component({
  selector: 'app-liste-categorie',
  templateUrl: './liste-categorie.component.html',
  styleUrls: ['./liste-categorie.component.css']
})
export class ListeCategorieComponent implements OnInit {

  constructor(private listeCategorieService: ListeCategorieService) { }

  categories: Array<Categorie>;
  displayedColumns: string[] = ['nom', 'nb_produits', 'actions'];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  ngOnInit() {
    this.listeCategorieService.getAll().subscribe(data => {
      this.categories = data;
      this.dataSource = new MatTableDataSource(this.categories)
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utilisateur } from '../model/utilisateur';
import { Token } from '../model/token';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private url = 'http://localhost:8084';

  constructor(private http: HttpClient) { }

  login(user: Utilisateur): Observable<Token> {
    return this.http.post<Token>(`http://localhost:8084/api/login`, user);
  }
}

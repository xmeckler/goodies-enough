import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Utilisateur} from '../../model/Utilisateur';

@Injectable({
  providedIn: 'root'
})
export class ListeUtilisateurService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get('//localhost:8084/api/admin/users');
  }
}

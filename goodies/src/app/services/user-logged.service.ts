import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utilisateur } from '../model/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class UserLoggedService {

  constructor(private http: HttpClient) { }

  getUser(): Observable<any> {
    return this.http.get('http://localhost:8084/api/estConnecte');
  }

  save(user: Utilisateur): Observable<any> {
    let result: Observable<Object>;
      result = this.http.post('http://localhost:8084/api/users', user);

    return result;
  }
}

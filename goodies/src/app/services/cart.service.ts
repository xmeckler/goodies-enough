import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Panier } from '../model/panier';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  // infoPanier = null;

  constructor(private http: HttpClient) { }

  addItem(panier: Panier): Observable<Panier> {
    const obs = this.http.post<Panier>('//localhost:8084/api/cart', panier);
    obs.subscribe(obj => {
      localStorage.removeItem('commandeInfos');
      localStorage.setItem('commandeInfos', JSON.stringify(obj.commandePanier));
    });
    return obs;
  }

  findItemsInCart(id): Observable<any> {
    return this.http.get('//localhost:8084/api/cart/' + id);
  }

}

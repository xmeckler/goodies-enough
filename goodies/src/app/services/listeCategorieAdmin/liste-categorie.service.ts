import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Produit} from '../../model/produit';
import {Categorie} from '../../model/categorie';

@Injectable({
  providedIn: 'root'
})
export class ListeCategorieService {

  private categorieUrl = '//localhost:8084/api/admin/categories';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get<Produit[]>('//localhost:8084/api/tests/categories');
  }

  public createCategorie(categorie) {
    return this.http.post<Categorie>(this.categorieUrl, categorie);
  }
}

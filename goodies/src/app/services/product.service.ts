import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  findHomepageProducts(): Observable<any> {
    return this.http.get('//localhost:8084/api/homepage');
  }

  findAll(): Observable<any> {
    return this.http.get('//localhost:8084/api/catalog');
  }

  findProduitsCategorie(id): Observable<any> {
    return this.http.get('//localhost:8084/api/catalog/prout/' + id);
  }

  findById(id): Observable<any> {
    return this.http.get('//localhost:8084/api/catalog/produit/prout/' + id);
  }
}

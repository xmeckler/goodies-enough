import { TestBed } from '@angular/core/testing';

import { ListeCommandeService } from './liste-commande.service';

describe('ListeCommandeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListeCommandeService = TestBed.get(ListeCommandeService);
    expect(service).toBeTruthy();
  });
});

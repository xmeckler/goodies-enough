
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Produit} from '../../model/produit';

@Injectable({
  providedIn: 'root'
})
export class ListeProduitService {


  constructor(private http: HttpClient) { }

  private produitUrl = '//localhost:8084/api/admin/product';

  getAll(): Observable<any> {
    return this.http.get('//localhost:8084/api/admin/products');
  }

  public deleteProduit(produit) {
    return this.http.delete(this.produitUrl + '/' + produit.id);
  }

  public createProduit(produit) {
    return this.http.post<Produit>(this.produitUrl, produit);
  }

}

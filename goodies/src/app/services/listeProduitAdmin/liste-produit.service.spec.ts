import { TestBed } from '@angular/core/testing';

import { ListeProduitService } from './liste-produit.service';

describe('ListeProduitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListeProduitService = TestBed.get(ListeProduitService);
    expect(service).toBeTruthy();
  });
});

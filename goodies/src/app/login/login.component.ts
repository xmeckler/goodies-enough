import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../services/auth.service';
import { Token } from '../model/token';
import { Utilisateur } from '../model/utilisateur';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   user: Utilisateur;

  constructor(
  private router: Router,
  private authService: AuthorizationService) {

  }

  ngOnInit() {
    sessionStorage.clear();
    this.user = new Utilisateur();
  }
/*
  login() {
       let url = 'http://localhost:8084/api/login';
       let result = this.http.post<Observable<boolean>>(url, {
          email: this.model.email,
          motDePasse: this.model.motDePasse
      }).subscribe(
      data => {
        this.token.saveToken(data.token);
        this.router.navigate(['user']);
      }
    );
   }
   */

  login() {
  this.authService.login(this.user).subscribe((tokens: Token) => {
      if (tokens.tokenAdmin) {
          sessionStorage.setItem('tokenAdmin', tokens.tokenAdmin);
          this.router.navigate(['']);
          //alert('Connexion ok');
        } else if (tokens.tokenUser) {
        sessionStorage.setItem('tokenUser', tokens.tokenUser);
        this.router.navigate(['']);
        //alert('Connexion ok');
      } else {
          alert('Echec');
      }
    }, err => {
      alert(err.error.message);
    }
  );
}

}

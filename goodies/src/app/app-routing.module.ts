import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Génériques */
import { HomepageComponent } from './homepage/homepage.component';

/* Routes statiques */
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { ContactComponent } from './contact/contact.component';
import { SupportComponent } from './support/support.component';
import { FaqComponent } from './faq/faq.component';
import { CgvComponent } from './cgv/cgv.component';

/* Routes de formulaires */
import { RetrievePasswordComponent } from './retrieve-password/retrieve-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';

/* Routes du catalogue */
import { CatalogProductComponent } from './catalog-product/catalog-product.component';
import { LeaveReviewComponent } from './leave-review/leave-review.component';
import { CatalogComponent } from './catalog/catalog.component';

/* Routes du panier */
import { CartDetailsComponent } from './cart-details/cart-details.component';
import { CartThanksComponent } from './cart-thanks/cart-thanks.component';

/* Routes pour l'utilisateur */
import { UserAccountEditComponent } from './user-account-edit/user-account-edit.component';
import { UserAccountComponent } from './user-account/user-account.component';

/* Routes pour l'outil d'administration */
import { CategoriesComponent }       from './admin-goodies/categories/categories.component';
import { ProductsComponent }         from './admin-goodies/products/products.component';
import { OrdersComponent }           from './admin-goodies/orders/orders.component';
import { UsersComponent }            from './admin-goodies/users/users.component';
import {BackendComponent} from './backendComponents/backend.component';
import {ListeProduitComponent} from './backendComponents/produit/liste-produit/liste-produit.component';
import {AddProduitComponent} from './backendComponents/produit/add-produit/add-produit.component';
import {ListeCategorieComponent} from './backendComponents/categorie/liste-categorie/liste-categorie.component';
import {ListeCommandeComponent} from './backendComponents/commande/liste-commande/liste-commande.component';
import {ViewCommandeComponent} from './backendComponents/commande/view-commande/view-commande.component';
import {ListeUserComponent} from './backendComponents/user/liste-utilisateur/liste-user.component';


const routes: Routes = [
        /* Génériques */
        {path: '', component: HomepageComponent},

        /* Routes statiques */
        {path: 'legales', component: MentionsLegalesComponent},
        {path: 'contact', component: ContactComponent},
        {path: 'support', component: SupportComponent},
        {path: 'faq', component: FaqComponent},
        {path: 'cgv', component: CgvComponent},

        /* Routes de formulaires */
        {path: 'retrieve-password', component: RetrievePasswordComponent},
        {path: 'reset-password', component: ResetPasswordComponent},
        {path: 'signup', component: SignupComponent},
        {path: 'login', component: LoginComponent},

        /* Routes du catalogue */
        {path: 'catalogue/produit/:nomProduit/:idProduit', component: CatalogProductComponent},
        {path: 'catalogue/:nomCategorie/:idCategorie', component: CatalogComponent},
        {path: 'catalogue/produit/commantaire', component: LeaveReviewComponent},
        {path: 'catalogue/produit', component: CatalogProductComponent},
        {path: 'catalogue', component: CatalogComponent},

        /* Routes du panier */
        {path: 'panier', component: CartDetailsComponent},
        {path: 'merci', component: CartThanksComponent},

        /* Routes pour l'utilisateur */
        {path: 'mon-compte/modifier', component: UserAccountEditComponent},
        {path: 'mon-compte', component: UserAccountComponent},

    /* Routes pour l'outil d'administration */
{path: 'admin-goodies/manage-categories', component: CategoriesComponent},
{path: 'admin-goodies/manage-products', component: ProductsComponent},
{path: 'admin-goodies/manage-orders', component: OrdersComponent},
{path: 'admin-goodies/manage-users', component: UsersComponent},
{path: 'admin/manage-products', component: ListeProduitComponent},
{path: 'admin/add-product', component: AddProduitComponent},
{path: 'admin/manage-categories', component: ListeCategorieComponent},
{path: 'admin/manage-orders', component: ListeCommandeComponent},
{path: 'admin/view-order', component: ViewCommandeComponent},
{path: 'admin/manage-users', component: ListeUserComponent}

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

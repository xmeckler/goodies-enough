export class Produit {
	 id: number ;
	 nom: string;
	 image: string ;
	 description: string;
	 prix: number;
	 promo: boolean;
	 nbvente: number;
	 surCommande: boolean ;
	 quantiteStock: number ;
	 categorieProduit: number;
}

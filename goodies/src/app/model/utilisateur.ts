import { UtilisateurAdresse } from './utilisateur-adresse';

export class Utilisateur {
  id: number;
  nom: string;
  prenom: string;
  email: string;
  numTelephone: string;
  motDePasse: string;
  utilisateurAdresse: UtilisateurAdresse;
  dateInscription = new Date();
}

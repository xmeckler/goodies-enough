import { UtilisateurAdresse } from './utilisateur-adresse';

export class UtilisateurVueSimple {
  id: number;
  nom: string;
  prenom: string;
  email: string;
  numTelephone: string;
  utilisateurAdresse: UtilisateurAdresse ;
  dateInscription = new Date();
}

import {Utilisateur} from './utilisateur';

export class Commande {
  id: number;
  dateCommande = new Date();
  datePaiement = new Date();
  dateValide = new Date();
  dateRefus = new Date();
  numCommande: string;
  nbProduit: number;
  montantTotal: number;
  contenuCommande: string;
  modePaiement: string;
  motifRefus: string;
  statutCommande: boolean
  utilisateurCommande: Utilisateur;
}

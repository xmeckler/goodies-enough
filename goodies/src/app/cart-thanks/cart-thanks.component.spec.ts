import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartThanksComponent } from './cart-thanks.component';

describe('CartThanksComponent', () => {
  let component: CartThanksComponent;
  let fixture: ComponentFixture<CartThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartThanksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

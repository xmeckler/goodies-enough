import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Produit } from '../model/produit';

@Component({
  selector: 'homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  products: Array<any>;
  mainItem: Produit;

  constructor(private productService: ProductService) {
    this.refreshContent();
  }

  ngOnInit() {
    this.refreshContent();
  }

  refreshContent() {
    // Récupération de la liste de tous les produits en promo et aléatoire
    this.productService.findHomepageProducts().subscribe(data => {
      this.mainItem = Object.assign([], data)[0];
      this.products = data.slice(1, 4);
    });
  }

}

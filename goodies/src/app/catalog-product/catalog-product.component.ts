import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';
import { CartService } from '../services/cart.service';
import { Panier } from '../model/panier';

@Component({
  selector: 'catalog-product',
  templateUrl: './catalog-product.component.html',
  styleUrls: ['./catalog-product.component.css']
})
export class CatalogProductComponent implements OnInit {
  public idProduit: string;
  private paramObs: any;
  private monPanier: Panier = new Panier();
  produit: any;
  commande: any;
  quantite: number;

  // loadData = async () => this.name = await this.productService.findById(this.idProduit);

  constructor(private route: ActivatedRoute, private productService: ProductService, private cartService: CartService) {
    route.params.subscribe(idProduit => {
      this.displayContent();
      this.quantite = 1;
    });
    // this.loadData();
  }

  ngOnInit() {
    this.displayContent();
  }

  addCart(event) {
    event.preventDefault();

    if (localStorage.getItem('commandeInfos') === null) {
      this.commande = null;
    } else {
      this.commande = JSON.parse(localStorage.getItem('commandeInfos'));
    }

    this.monPanier.quantite = this.quantite;
    this.monPanier.produitPanier = this.produit;
    this.monPanier.commandePanier = this.commande;

    this.cartService.addItem(this.monPanier).subscribe();

  }

  displayContent() {
    // Récupération de l'id du produit depuis l'URL
    this.paramObs = this.route.params.subscribe(params => {
      this.idProduit = this.route.snapshot.paramMap.get('idProduit');
    });

    // Récupération du produit
    this.productService.findById(this.idProduit).subscribe(data => {
      data.average = (data.avis.reduce((tot, avi) => tot  + avi.note, 0) / data.avis.length);
      data.nb5Stars = data.avis.filter(avi5 => avi5.note === 5).length;
      data.nb4Stars = data.avis.filter(avi4 => avi4.note === 4).length;
      data.nb3Stars = data.avis.filter(avi3 => avi3.note === 3).length;
      data.nb2Stars = data.avis.filter(avi2 => avi2.note === 2).length;
      data.nb1Stars = data.avis.filter(avi1 => avi1.note === 1).length;
      this.produit = data;
    });
  }
}

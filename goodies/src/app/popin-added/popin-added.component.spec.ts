import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopinAddedComponent } from './popin-added.component';

describe('PopinAddedComponent', () => {
  let component: PopinAddedComponent;
  let fixture: ComponentFixture<PopinAddedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopinAddedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopinAddedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

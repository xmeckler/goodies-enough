import { LayoutModule } from '@angular/cdk/layout';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatToolbarModule,
} from '@angular/material';

import { NavGoodiesEnoughComponent } from './nav-goodies-enough.component';

describe('NavGoodiesEnoughComponent', () => {
  let component: NavGoodiesEnoughComponent;
  let fixture: ComponentFixture<NavGoodiesEnoughComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavGoodiesEnoughComponent],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        MatToolbarModule,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavGoodiesEnoughComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});

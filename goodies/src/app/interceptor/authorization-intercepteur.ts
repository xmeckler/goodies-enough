import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {



    getAuthorizationToken(): string {
        if (sessionStorage.getItem('tokenAdmin')) {
          return `Bearer ${sessionStorage.getItem('tokenAdmin')}`;
        } else if (sessionStorage.getItem('tokenUser')) {
          return `Bearer ${sessionStorage.getItem('tokenUser')}`;
        } else {
          return '';
        }
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        const authToken = this.getAuthorizationToken();

        const authReq = req.clone({
          headers: req.headers.set('Authorization', authToken)
        });

        return next.handle(authReq);
  }
}

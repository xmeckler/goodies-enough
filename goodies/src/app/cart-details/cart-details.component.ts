import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { Panier } from '../model/panier';

@Component({
  selector: 'cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.css']
})
export class CartDetailsComponent implements OnInit {

  panier: Array<any>;
  commande = JSON.parse(localStorage.getItem('commandeInfos'));

  breadcrumb = 'Votre panier';
  get BreadCrumb() {
    return this.breadcrumb;
  }

  constructor(private cartService: CartService) {
    this.displayContent();
  }

  ngOnInit() {
    this.displayContent();
  }

  displayContent() {
    console.log(this.commande.id);
    this.cartService.findItemsInCart(this.commande.id).subscribe(data => {
      data.totalPanier = Object.assign([], data).reduce((acc, item) => acc + (item.quantite * item.produitPanier.prix), 0);
      this.panier = data;
    });
  }

}

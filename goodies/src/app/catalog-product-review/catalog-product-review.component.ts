import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'catalog-product-review',
  templateUrl: './catalog-product-review.component.html',
  styleUrls: ['./catalog-product-review.component.css']
})
export class CatalogProductReviewComponent implements OnInit {

  @Input() prop1: any;
  constructor() { }

  ngOnInit() {
    console.log(this.prop1);
  }

}

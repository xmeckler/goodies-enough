import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogProductReviewComponent } from './catalog-product-review.component';

describe('CatalogProductReviewComponent', () => {
  let component: CatalogProductReviewComponent;
  let fixture: ComponentFixture<CatalogProductReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogProductReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogProductReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

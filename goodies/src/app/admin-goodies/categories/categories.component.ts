import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'admin-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  leLaL = 'la ';
  unUne = 'une';
  deLeDu = 'de la ';
  ceCette = 'cette';

  objet = 'catégorie';
  objets = 'catégories';

  constructor() { }

  ngOnInit() {
  }

}

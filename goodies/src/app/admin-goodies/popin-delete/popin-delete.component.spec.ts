import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopinDeleteComponent } from './popin-delete.component';

describe('PopinDeleteComponent', () => {
  let component: PopinDeleteComponent;
  let fixture: ComponentFixture<PopinDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopinDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopinDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

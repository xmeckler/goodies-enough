import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopinCancelComponent } from './popin-cancel.component';

describe('PopinCancelComponent', () => {
  let component: PopinCancelComponent;
  let fixture: ComponentFixture<PopinCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopinCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopinCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
